import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

export const AppRoutes: Routes = [
    {
        path: 'home',
        loadChildren: () => import('@app/modules/home/home.module').then(m => m.HomeModule)
    },
    {
        path: 'products',
        loadChildren: () => import('@app/modules/product/product.module').then(m => m.ProductModule)
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }
];


@NgModule({
    imports: [RouterModule.forRoot(AppRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}