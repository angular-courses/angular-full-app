import { Subscription } from 'rxjs/Subscription';
import { ProductsService } from '@app/modules/product/services/products.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';


@Component({
  // tslint:disable-next-line: component-selector
  selector: 'product-list',
  templateUrl: './product-list.component.html'
})
export class ProductListComponent implements OnInit, OnDestroy {

  productName = 'A Book';
  products = [];
  private productsSubscription: Subscription;

  constructor(private productsService: ProductsService) {}

  ngOnInit () {
    this.products = this.productsService.getProducts();

    this.productsSubscription = this.productsService.productsUpdated.subscribe({
      complete: () => this.products = this.productsService.getProducts()
    });
  }

  onAddProduct(form: NgForm) {
    if (form.valid) {
      // this.products.push(form.value.productName);
      this.productsService.addProduct(form.value.productName);
    }
  }

  onRemoveProduct(productName: string) {
    this.productsService.deleteProduct(productName);
  }

  ngOnDestroy() {
    this.productsSubscription.unsubscribe();
  }
}
