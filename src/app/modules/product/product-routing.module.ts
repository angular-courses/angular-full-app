import { ProductListComponent } from './presentational/product-list/product-list.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

export const ProductRoutes: Routes = [
    {
        path: '',
        component: ProductListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(ProductRoutes)],
    exports: [RouterModule]
})
export class ProductRoutingModule {

}